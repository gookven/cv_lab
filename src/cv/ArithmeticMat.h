#pragma once

#include <range/v3/all.hpp>
#include <type_traits>

#include "Mat.h"

namespace cv
{

template<typename T>
class ArithmeticMat;

using FloatMat = ArithmeticMat<float>;
using IntMat = ArithmeticMat<int>;

FloatMat operator^(const FloatMat& a, const FloatMat& b);

FloatMat GradientDirection(const FloatMat& matx, const FloatMat& maty);

template<typename T>
class ArithmeticMat : public Mat<T>
{
    // http://en.cppreference.com/w/cpp/types/is_arithmetic
    // Arithmetic types are the types for which the built-in arithmetic operators
    // (+, -, *, /) are defined (possibly in combination with the usual arithmetic conversions)
    static_assert(std::is_arithmetic<T>::value,
                  "Arithmetic type required for instantiation. Define (+, -, *, /) operations to use your custom type!");

public:
    using Mat<T>::getRows;
    using Mat<T>::getCols;
    using Mat<T>::at;
    using Mat<T>::operator[];
    using Mat<T>::begin;
    using Mat<T>::end;

    ArithmeticMat();

    ArithmeticMat(int _rows, int _cols, T* _data);

    ArithmeticMat(int _rows, int _cols);

    ArithmeticMat(int _cols);

    ArithmeticMat(int _rows, int _cols, const std::initializer_list<T> _list);

    ArithmeticMat operator*(const ArithmeticMat& other) const;

    ArithmeticMat operator*(const T x) const;

    ArithmeticMat operator+(const T x) const;

    ArithmeticMat operator+(const ArithmeticMat& other) const;

    ArithmeticMat operator-(const ArithmeticMat& other) const;

    const ArithmeticMat& operator=(const T x);

    T module() const;
};


template<typename T>
ArithmeticMat<T> ArithmeticMat<T>::operator*(const ArithmeticMat<T>& other) const
{
    assert(getCols() == other.getRows());

    ArithmeticMat<T> res(getRows(), other.getCols());
    res = 0.0f;

    int n = getCols();
    int rows = res.getRows(), cols = res.getCols();

    for (int i = 0; i < rows; ++i)
    {
        for (int k = 0; k < n; ++k)
        {
            float aik = at(i, k);
            for (int j = 0; j < cols; ++j)
            {
                res.at(i, j) += aik * other.at(k, j);
            }
        }
    }

    return res;
}

template<typename T>
ArithmeticMat<T> ArithmeticMat<T>::operator*(const T y) const
{
    ArithmeticMat<T> res(getRows(), getCols());
    std::transform(begin(), end(), res.begin(), [=](const T& x)
    {return x * y;});
    return res;
}

template<typename T>
ArithmeticMat<T> ArithmeticMat<T>::operator+(const T y) const
{
    ArithmeticMat<T> res(getRows(), getCols());
    std::transform(begin(), end(), res.begin(), [=](const T& x)
    {return x + y;});
    return res;
}

template<typename T>
ArithmeticMat<T> ArithmeticMat<T>::operator+(const ArithmeticMat<T>& other) const
{
    assert(getRows() == other.getRows());
    assert(getCols() == other.getCols());

    ArithmeticMat<T> res(getRows(), getCols());
    std::transform(begin(), end(), other.begin(), res.begin(), [](const T& x, const T& y)
    {return x + y;});

    return res;
}

template<typename T>
ArithmeticMat<T> ArithmeticMat<T>::operator-(const ArithmeticMat<T>& other) const
{
    assert(getRows() == other.getRows());
    assert(getCols() == other.getCols());

    ArithmeticMat<T> res(getRows(), getCols());
    std::transform(begin(), end(), other.begin(), res.begin(), [](const T& x, const T& y)
    {return x - y;});

    return res;
}

template<typename T>
ArithmeticMat<T>::ArithmeticMat(int _rows, int _cols, T* _data)
        : Mat<T>(_rows, _cols, _data)
{}

template<typename T>
ArithmeticMat<T>::ArithmeticMat(int _rows, int _cols)
        : ArithmeticMat(_rows, _cols, new T[_rows * _cols])
{}

template<typename T>
ArithmeticMat<T>::ArithmeticMat(int _cols)
        : ArithmeticMat(1, _cols)
{

}

template<typename T>
ArithmeticMat<T>::ArithmeticMat(int _rows, int _cols, const std::initializer_list<T> _list)
        : Mat<T>(_rows, _cols, _list)
{}

template<typename T>
ArithmeticMat<T>::ArithmeticMat()
        : Mat<T>(1, 1, {T{}})
{}

template<typename T>
const ArithmeticMat<T>& ArithmeticMat<T>::operator=(const T x)
{
    std::fill(begin(), end(), x);
    return *this;
}

template<typename T>
T ArithmeticMat<T>::module() const
{
    return std::inner_product(begin(), end(), begin(), T{});
}


}// namespace cv;



