#pragma once

#include "Kernel.h"
#include "FeatureDetection.h"

namespace cv
{

namespace descriptor_extraction
{

using namespace feature_detection;

class Descriptor : public FloatMat
{
public:
    using FloatMat::operator=;
    using FloatMat::module;
    using FloatMat::begin;
    using FloatMat::end;
    // actually compare data pointers
    using Mat::operator==;

    Feature feature;
    float angle;

    Descriptor(int _length, const Feature& _feature = Feature())
            : FloatMat(_length), feature(_feature)
    {operator=(0.0f);}

    const Descriptor& normalize()
    {
        auto a = 1.0f / std::sqrt(module());
        for (auto&& x : *this)
        {
            x *= a;
        }
        return *this;
    }

    float distance(const Descriptor& other) const
    {
        assert(size() == other.size() && "Descriptor sizes must be equal");
        return std::sqrt(
                std::inner_product(begin(), end(), other.begin(), 0.0f, std::plus<float>(), [](float a, float b)
                {
                    auto x = a - b;
                    return x * x;
                })
        );
    }
};

inline std::ostream& operator<<(std::ostream& out, const Descriptor& desc)
{
    out << "angle: " << desc.angle << " [ ";
    for (auto&& x : desc)
    {
        out << x << " ";
    }
    out << " ]" << std::endl;
    return out;
}

using DescriptorPair = std::pair<Descriptor, Descriptor>;
using DescriptorList = std::vector<Descriptor>;
using DescriptorPairList = std::vector<DescriptorPair>;

DescriptorList ExtractDescriptors(const FloatMat& _floatMat, const FeatureList& _featureList,
                                  int _subDescriptorLength = 32, int _subWindowsPerLine = 5, int _subWindowSize = 20);

Descriptor ExtractDescriptor(const vec2i& _center, float _rotateAngle, const vec2i& _basePos, int _windowSize,
                             const vec2i& _wrapper, int _descriptorLength,
                             const FloatMat& _angles, const FloatMat& _magnitudes);

DescriptorPair GetTwoClosestDescriptors(const Descriptor& _desc, const DescriptorList& _list);

DescriptorPairList GetCorrespondingPairs(const DescriptorList& _firstList, const DescriptorList& _secondList,
                                         float _threshold = 0.8f);

std::vector<float> ExtractDirections(const Descriptor& _descriptor);

float GetInterpolatedXAxisCenter(const std::array<float, 3>& x, const std::array<float, 3>& y, float sqXStep);

}// namespace descriptor_extraction;

}// namespace cv;



