#pragma once

#include <memory>
#include <cassert>
#include <QImage>

#include "common.h"

namespace cv
{

template<typename T>
class Mat;

using ImageMat = Mat<RGBA>;

template<typename T>
class Mat
{
private:
    std::shared_ptr<T> data;
    int rows;
    int cols;

    Mat()
    {};

public:

    Mat(int _rows, int _cols, T* _data)
            : rows(_rows), cols(_cols), data(std::shared_ptr<T>(_data, std::default_delete<T[]>()))
    {}

    Mat(int _rows, T* _data)
            : Mat(_rows, 1, _data)
    {}

    Mat(int _rows, int _cols)
            : Mat(_rows, _cols, new T[_rows * _cols])
    {}

    Mat(int _rows)
            : Mat(_rows, 1, new T[_rows])
    {}

    inline T& at(int i) const
    {
        return *(data.get() + i);
    }

    inline T& at(int i, int j) const
    {
        return at(i * cols + j);
    }

    inline T& at(const vec2i& v) const
    {
        return at(v.i, v.j);
    }

    inline T& operator[](int i) const
    {
        return at(i);
    }

    inline T& at(int i)
    {
        return *(data.get() + i);
    }

    inline T& at(int i, int j)
    {
        return at(i * cols + j);
    }

    inline T& at(const vec2i& v)
    {
        return at(v.i, v.j);
    }

    inline T& operator[](int i)
    {
        return at(i);
    }

    inline int getRows() const
    {
        return rows;
    }

    inline int getCols() const
    {
        return cols;
    }

    int size() const
    {
        return rows * cols;
    }

    inline T* begin()
    {
        return data.get();
    }

    inline T* end()
    {
        return data.get() + size();
    }

    inline T* begin() const
    {
        return data.get();
    }

    inline T* end() const
    {
        return data.get() + size();
    }

    bool operator==(const Mat<T>& other) const
    {
        return data == other.data;
    }

    Mat& operator=(const std::initializer_list<T> list)
    {
        std::copy(list.begin(), list.end(), begin());
        return *this;
    }

    Mat(int _rows, int _cols, const std::initializer_list<T> list)
            : Mat(_rows, _cols, new T[_rows * _cols])
    {
        operator=(list);
    }

    void copyData(const Mat<T>& other)
    {
        assert(size() == other.size());
        std::copy(other.begin(), other.end(), begin());
    }

};

}// namespace cv;
