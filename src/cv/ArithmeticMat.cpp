#include "ArithmeticMat.h"

namespace cv
{

FloatMat operator^(const FloatMat& a, const FloatMat& b)
{
    assert(a.getRows() == b.getRows());
    assert(a.getCols() == b.getCols());

    FloatMat res(a.getRows(), a.getCols());
    std::transform(a.begin(), a.end(), b.begin(), res.begin(), [](const float& x, const float& y)
    {return sqrtf(x * x + y * y);});

    return res;
}

FloatMat GradientDirection(const FloatMat& matx, const FloatMat& maty)
{
    assert(matx.getRows() == maty.getRows());
    assert(matx.getCols() == maty.getCols());

    FloatMat res(matx.getRows(), maty.getCols());
    std::transform(matx.begin(), matx.end(), maty.begin(), res.begin(), [](const float& x, const float& y)
    {return atan2f(y, x);});

    return res;
}

}// namespace cv;