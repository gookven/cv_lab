#include <QtCore/qdir.h>
#include <range/v3/all.hpp>

#include "Utils.h"

namespace cv
{

FloatMat operator*(const FloatMat& _mat, const SeparableKernel& _separableKernel)
{
    return _mat * _separableKernel.first * _separableKernel.second;
}

namespace utils
{

using namespace ranges;

SeparableKernel GaussianBlurSeparable(float _smoothness)
{
    assert(_smoothness >= 0.0f);
    int filterRadius = (int) (_smoothness * 3.0f);
    int filterSize = filterRadius * 2 + 1;

    Kernel first(filterSize, 1);
    Kernel second(1, filterSize);

    float smooth = _smoothness * _smoothness;

    for (int i : view::ints(0, filterSize))
    {
        int ii = i - filterRadius;
        float res = 1.0f / (M_SQRT_2_PI_F * smooth)
                    * powf(M_E_F, -(ii * ii) / (2.0f * smooth));
        first.at(i) = second.at(i) = res;
    }

    return SeparableKernel(first.normalize(), second.normalize());
}

Kernel GaussianBlur(float _smoothness)
{
    assert(_smoothness >= 0.0f);
    int filterRadius = (int) (_smoothness * 3.0f);
    int filterSize = filterRadius * 2 + 1;

    Kernel res(filterSize, filterSize);

    float smooth = _smoothness * _smoothness;

    for (int x : view::ints(0, filterSize))
    {
        int xx = x - filterRadius;
        for (int y : view::ints(0, filterSize))
        {
            int yy = y - filterRadius;
            res.at(y, x) = 1.0f / (M_2_PI_F * smooth)
                           * powf(M_E_F, -(xx * xx + yy * yy) / (2.0f * smooth));
        }
    }

    return res.normalize();
}

FloatMat ToFloatMat(const ImageMat& mat)
{
    FloatMat res(mat.getRows(), mat.getCols());
    std::transform(mat.begin(), mat.end(), res.begin(), [](const RGBA pixel)
    {return pixel.intensity();});
    return res;
}

ImageMat ToImageMat(const FloatMat& mat)
{
    ImageMat res(mat.getRows(), mat.getCols());
    std::transform(mat.begin(), mat.end(), res.begin(), [](const float value)
    {return RGBA::fromIntensity(value);});
    return res;
}

ImageMat FromQImage(const QImage& image)
{
    int rows = image.height(), columns = image.width();

    ImageMat mat(rows, columns);

    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < columns; ++j)
        {
            QRgb p = image.pixel(j, i);
            mat.at(i, j) = RGBA{(unsigned char) qRed(p), (unsigned char) qGreen(p), (unsigned char) qBlue(p),
                                (unsigned char) qAlpha(p)};
        }
    }

    return mat;
}

QImage ToQImage(const ImageMat& mat)
{
    int rows = mat.getRows(), cols = mat.getCols();

    QImage image(cols, rows, QImage::Format_RGBA8888);

    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            RGBA p = mat.at(i, j);
            image.setPixel(j, i, qRgba(p.r, p.g, p.b, p.a));
        }
    }

    return image;
}

QImage ToQImage(const FloatMat& mat)
{
    return ToQImage(ToImageMat(mat));
}

FloatMat& Normalize(const FloatMat& mat, FloatMat& store)
{
    assert(mat.getRows() == store.getRows());
    assert(mat.getCols() == store.getCols());

    auto minmax = std::minmax_element(mat.begin(), mat.end());
    float min = *minmax.first;
    float d = *minmax.second - min;

    std::transform(mat.begin(), mat.end(), store.begin(), [=](const float& x)
    {return (x - min) / d;});

    return store;
}

FloatMat Normalize(const FloatMat& mat)
{
    FloatMat res(mat.getRows(), mat.getCols());
    return Normalize(mat, res);
}

bool labDirExists(int dir, const QFileInfo& fileInfo)
{
    return QDir{}.exists(QString("%1/%2/").arg(fileInfo.absolutePath(), QString::number(dir)));
}

bool createLabPath(int dir, const QFileInfo& fileInfo)
{
    return QDir{}.mkdir(QString("%1/%2/").arg(fileInfo.absolutePath(), QString::number(dir)));
}

QString insertId(int id, int dir, const QFileInfo& fileInfo)
{
    return QString("%1/%2/%3_%4")
            .arg(fileInfo.absolutePath(), QString::number(dir), QString::number(id), fileInfo.fileName());
}

}// namespace utils;

}// namespace cv;
