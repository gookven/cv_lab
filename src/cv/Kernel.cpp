#include "Kernel.h"

namespace cv
{

Kernel::Kernel(int _rows, int _cols, float* _data)
        : FloatMat(_rows, _cols, _data)
          , rowOffset(_rows / 2)
          , colOffset(_cols / 2)
{
    assert(getRows() % 2);
    assert(getCols() % 2);
}

FloatMat& Kernel::apply(const FloatMat& mat, FloatMat& store, EdgeHandling edgeHandling) const
{
    int h = mat.getRows();
    int w = mat.getCols();

    assert(h == store.getRows());
    assert(w == store.getCols());

    FloatMat res = store;
    if (store == mat)
    {
        // can't perform operation in-place, need temporary storage
        res = FloatMat(h, w);
    }

    int krows = getRows();
    int kcols = getCols();

    switch (edgeHandling)
    {
        case EdgeHandling::Extend:
        {
            for (int i = 0; i < h; ++i)
            {
                for (int j = 0; j < w; ++j)
                {
                    float sum = 0.0f;
                    for (int ki = 0; ki < krows; ++ki)
                    {
                        int mi = std::min(std::max(0, i - rowOffset + ki), h - 1);
                        for (int kj = 0; kj < kcols; ++kj)
                        {
                            int mj = std::min(std::max(0, j - colOffset + kj), w - 1);
                            sum += at(ki, kj) * mat.at(mi, mj);
                        }
                    }
                    res.at(i, j) = sum;
                }
            }
        };
            break;

        case EdgeHandling::Wrap:
        {
            for (int i = 0; i < h; ++i)
            {
                for (int j = 0; j < w; ++j)
                {
                    float sum = 0.0f;
                    for (int ki = 0; ki < krows; ++ki)
                    {
                        int mi = ((i - rowOffset + ki) % h + h) % h;
                        for (int kj = 0; kj < kcols; ++kj)
                        {
                            int mj = ((j - colOffset + kj) % h + w) % w;
                            sum += at(ki, kj) * mat.at(mi, mj);
                        }
                    }
                    res.at(i, j) = sum;
                }
            }
        };
            break;

        case EdgeHandling::Crop:
        {
            for (int i = 0; i < h; ++i)
            {
                for (int j = 0; j < w; ++j)
                {
                    float sum = 0.0f;
                    for (int ki = 0; ki < krows; ++ki)
                    {
                        int mi = i - rowOffset + ki;
                        if (mi < 0 || mi >= h) continue;
                        for (int kj = 0; kj < kcols; ++kj)
                        {
                            int mj = ((j - colOffset + kj) % h + w) % w;
                            if (mj < 0 || mj >= w) continue;
                            sum += at(ki, kj) * mat.at(mi, mj);
                        }
                    }
                    res.at(i, j) = sum;
                }
            }
        };
            break;

        case EdgeHandling::Reflect:
        {
            for (int i = 0; i < h; ++i)
            {
                for (int j = 0; j < w; ++j)
                {
                    float sum = 0.0f;
                    for (int ki = 0; ki < krows; ++ki)
                    {
                        int mi = ((i - rowOffset + ki) % h + h);
                        mi = (mi / h) & 1 ? h - 1 - mi % h : mi % h;
                        for (int kj = 0; kj < kcols; ++kj)
                        {
                            int mj = ((j - colOffset + kj) % w + w);
                            mj = (mj / w) & 1 ? w - 1 - mj % w : mj % w;
                            sum += at(ki, kj) * mat.at(mi, mj);
                        }
                    }
                    res.at(i, j) = sum;
                }
            }
        };
            break;
    }

    if (store == mat)
    {
        store.copyData(res);
    }

    return store;
}

Kernel& Kernel::normalize()
{
    float sum = std::accumulate(begin(), end(), 0.0f, [](const float& s, const float& x)
    {return s + std::fabs(x);});

    std::for_each(begin(), end(), [=](float& x)
    {x /= sum;});

    return *this;
}

Kernel Kernel::operator*(const Kernel& other) const
{
    Kernel res = FloatMat::operator*(other);
    return res;
}

FloatMat operator*(const FloatMat& mat, const Kernel& kernel)
{
    FloatMat result(mat.getRows(), mat.getCols());
    return kernel.apply(mat, result);
}

FloatMat& operator*=(FloatMat& mat, const Kernel& kernel)
{
    return kernel.apply(mat, mat);
}

Kernel::Kernel(int _rows, int _cols)
        : Kernel(_rows, _cols, new float[_rows * _cols])
{}

Kernel::Kernel(int _rows, int _cols, const std::initializer_list<float> list)
        : FloatMat(_rows, _cols, list)
          , rowOffset(_rows / 2)
          , colOffset(_cols / 2)
{
    assert(getRows() % 2);
    assert(getCols() % 2);
}

Kernel& Kernel::conv()
{
    int rows = getRows(), cols = getCols();

    for (int i = 0; i < rows / 2; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            std::swap(at(i, j), at(rows - i - 1, cols - j - 1));
        }
    }

    for (int j = 0; j < cols / 2; ++j)
    {
        std::swap(at(rows / 2, j), at(rows - rows / 2 - 1, cols - j - 1));
    }

    return *this;
}

Kernel::Kernel(FloatMat&& mat)
        : FloatMat(mat)
          , rowOffset(getRows() / 2)
          , colOffset(getCols() / 2)
{
    assert(getRows() % 2);
    assert(getCols() % 2);
}

}// namespace cv;