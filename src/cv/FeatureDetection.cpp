#include <cfloat>
#include <range/v3/all.hpp>
#include "FeatureDetection.h"
#include "Utils.h"

namespace cv
{

namespace feature_detection
{

bool Feature::operator<(const cv::feature_detection::Feature& _other) const
{
    return pos < _other.pos;
}

using namespace ranges;

const std::vector<vec2i> dt_directions
        {
                {-1, -1},
                {-1, 0},
                {-1, 1},
                {0,  -1},
                {0,  1},
                {-1, 1},
                {1,  -1},
                {1,  0},
                {1,  1}
        };

FeatureList GetMoravecFeatures(const FloatMat& _image, int _windowSize, float _minWeight)
{
    return GetFeatures(GetMoravecWeights(_image, _windowSize), _minWeight);
}

FloatMat GetMoravecWeights(const FloatMat& _image, int _windowSize)
{
    int height = _image.getRows(), width = _image.getCols();
    FloatMat weights(height, width);
    weights = FLT_MAX;

    vec2i wrapper(height, width);

    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            vec2i baseIndex(i - _windowSize / 2, j - _windowSize / 2);

            for (auto& dt : dt_directions)
            {

                float w = 0.0f;
                for (int ii = 0; ii < _windowSize; ++ii)
                {
                    for (int jj = 0; jj < _windowSize; ++jj)
                    {

                        vec2i ind1 = baseIndex + vec2i(ii, jj);
                        vec2i ind2 = ind1 + dt;

                        float diff = _image.at(ind1.wrap(wrapper)) - _image.at(ind2.wrap(wrapper));
                        w += diff * diff;
                    }
                }

                weights.at(i, j) = std::min(w, weights.at(i, j));
            }

        }

    }

    return weights;
}

FeatureList GetHarrisFeatures(const FloatMat& _image, int _windowSize, float _minWeight)
{
    return GetFeatures(GetHarrisWeights(_image, _windowSize), _minWeight);
}

FloatMat GetHarrisWeights(const FloatMat& _image, int _windowSize, float k)
{
    int height = _image.getRows(), width = _image.getCols();

    Kernel sobelX(3, 3, {
            -1.0f, 0.0f, 1.0f,
            -2.0f, 0.0f, 2.0f,
            -1.0f, 0.0f, 1.0f
    });
    Kernel sobelY(3, 3, {
            -1.0f, -2.0f, -1.0f,
            0.0f, 0.0f, 0.0f,
            1.0f, 2.0f, 1.0f
    });

    FloatMat ix = _image * sobelX;
    FloatMat iy = _image * sobelY;

    FloatMat ix2(height, width), iy2(height, width), ixy(height, width);

    std::transform(ix.begin(), ix.end(), ix2.begin(), [](float x)
    {return x * x;});
    std::transform(iy.begin(), iy.end(), iy2.begin(), [](float x)
    {return x * x;});
    std::transform(ix.begin(), ix.end(), iy.begin(), ixy.begin(), [](float x, float y)
    {return x * y;});

    FloatMat a(height, width), b(height, width), c(height, width);

    vec2i wrapper(height, width);
    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            vec2i baseIndex(i - _windowSize / 2, j - _windowSize / 2);

            for (int ii = 0; ii < _windowSize; ++ii)
            {
                for (int jj = 0; jj < _windowSize; ++jj)
                {
                    vec2i ind = (baseIndex + vec2i(ii, jj)).wrap(wrapper);

                    a.at(i, j) += ix2.at(ind);
                    b.at(i, j) += ixy.at(ind);
                    c.at(i, j) += iy2.at(ind);
                }
            }
        }
    }

    FloatMat weights(height, width);

    for (auto i : view::ints(0, height))
        for (auto j : view::ints(0, width))
        {
            auto sqrb = b.at(i, j) * b.at(i, j);
            auto sqrac = a.at(i, j) + c.at(i, j);
            weights.at(i, j) = a.at(i, j) * c.at(i, j) - sqrb - k * sqrac;
        }


    return weights;
}

FeatureList GetFeatures(const FloatMat& _weights, float _minWeight)
{
    int height = _weights.getRows(), width = _weights.getCols();

    FeatureList features;

    vec2i wrap{height, width};

    for (auto i : view::ints(0, height))
        for (auto j : view::ints(0, width))
            if (_weights.at(i, j) >= _minWeight && IsLocalMaximum(_weights, {i, j}, wrap))
                features.push_back({_weights.at(i, j), {i, j}});

    return features;
}

bool IsLocalMaximum(const FloatMat& _weights, const vec2i& _vec, const vec2i& _wrap)
{
    return std::none_of(dt_directions.begin(), dt_directions.end(), [=](const vec2i& dt)
    {
        return _weights.at((_vec + dt).wrap(_wrap)) > _weights.at(_vec);
    });
}

ImageMat MarkFeatures(const ImageMat& _image, const FeatureList& _features)
{
    int height = _image.getRows(), width = _image.getCols();
    ImageMat markedImage(height, width);
    markedImage.copyData(_image);

    vec2i wrapper(height, width);

    for (auto&& feature : _features)
    {
        for (auto&& dt : dt_directions)
        {
            auto ind = (feature.pos + dt).wrap(wrapper);
            markedImage.at(ind) = RGBA{255, 0, 0, 255};
        }
    }

    return markedImage;
}

FeatureList FilterFeatures(const FeatureList& _featureList, int _number)
{
    std::map<vec2i, Feature> featureMap;
    std::set<vec2i> toRemove;

    std::transform(_featureList.begin(), _featureList.end(), std::inserter(featureMap, featureMap.begin()),
            [](const Feature& f)
            {
                return std::make_pair(f.pos, f);
            });

    int radius = 0;
    while (featureMap.size() > _number)
    {
        toRemove.clear();
        int sqrRadius = radius * radius;

        for (auto&& featurePair : featureMap)
        {
            auto& pos = featurePair.first;
            auto& feature = featurePair.second;

            auto from = featureMap.lower_bound({pos.i - radius, 0});
            auto to = featureMap.upper_bound({pos.i + radius, 0});

            bool isBestFeatureInRadius = std::none_of(from, to, [=](const std::pair<vec2i, Feature>& other)
            {
                return pos.sqrDistance(other.first) <= sqrRadius && other.second.weight > feature.weight;
            });

            if (!isBestFeatureInRadius)
            {
                toRemove.insert(pos);
            }

            if (featureMap.size() - toRemove.size() <= _number) break;
        }

        for (auto&& removeKey : toRemove)
        {
            featureMap.erase(removeKey);
        }

        radius++;
    }

    FeatureList result;
    std::transform(featureMap.begin(), featureMap.end(), std::back_inserter(result),
            [](const std::pair<vec2i, Feature>& other)
            {
                return other.second;
            });
    return result;
}


}// namespace feature_detection;

}// namespace cv;