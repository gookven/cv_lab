#pragma once

namespace cv
{

enum class EdgeHandling
{
    Extend, Wrap, Crop, Reflect
};

static inline int wrapIndex(int _index, int _size, EdgeHandling _edgeHandling = EdgeHandling::Reflect) noexcept
{
    switch (_edgeHandling)
    {
        case EdgeHandling::Extend:
            return std::min(std::max(0, _index), _size - 1);
        case EdgeHandling::Wrap:
            return (_index % _size + _size) % _size;
        case EdgeHandling::Crop:
            if (_index < 0 || _index >= _size) return -1;
            return _index;
        case EdgeHandling::Reflect:
            auto norm = (_index % _size + _size);
            return (norm / _size) & 1 ? _size - 1 - norm % _size : norm % _size;
    }
}

struct vec2i
{
    const union
    {
        int i, y, h;
    };
    const union
    {
        int j, x, w;
    };

    vec2i()
            : i(0), j(0)
    {}

    vec2i(int _i, int _j)
            : i(_i), j(_j)
    {}

    inline vec2i operator+(const vec2i& b) const noexcept
    {
        return vec2i(i + b.i, j + b.j);
    }

    inline vec2i operator-(const vec2i& b) const noexcept
    {
        return vec2i(i - b.i, j - b.j);
    }

    inline vec2i operator+(const int t) const noexcept
    {
        return operator+({t, t});
    }

    inline vec2i operator-(const int t) const noexcept
    {
        return operator-({t, t});
    }

    inline vec2i operator/(const int t) const
    {
        return vec2i{i / t, j / t};
    }


    inline vec2i wrap(const vec2i& o, EdgeHandling _eh = EdgeHandling::Wrap) const noexcept
    {
        return vec2i(wrapIndex(i, o.h, _eh), wrapIndex(j, o.w, _eh));
    }

    inline vec2i rotate(float angle) const noexcept
    {
        float sin = std::sin(angle), cos = std::cos(angle);
        return {int(sin * x + cos * y), int(cos * x - sin * y)};
    }

    bool operator==(const vec2i& o) const noexcept
    {
        return i == o.i && j == o.j;
    }

    bool operator<(const vec2i& o) const noexcept
    {
        return i != o.i ? i < o.i : j < o.j;
    }

    int sqrDistance(const vec2i& o) const noexcept
    {
        int di = i - o.i;
        int dj = j - o.j;
        return di * di + dj * dj;
    }
};

using channel = unsigned char;

struct RGBA
{
    channel r, g, b, a;

    enum class IntensityType
    {
        PAL, SRGB
    };

    inline float intensity(IntensityType type = IntensityType::PAL) const noexcept;

    inline static RGBA fromIntensity(const float value) noexcept;
};

inline float RGBA::intensity(IntensityType type) const noexcept
{
    return (type == IntensityType::PAL ? (0.299f * r + 0.587f * g + 0.114f * b) : (0.213f * r + 0.715f * g +
                                                                                   0.072f * b)) /
           255.0f;
}

inline RGBA RGBA::fromIntensity(const float value) noexcept
{
    channel c = (channel) (255.0f * value);
    return RGBA{c, c, c, 255};
}

}// namespace cv;

