#pragma once

#include "ArithmeticMat.h"

namespace cv
{

class ScaleSpace
{
private:
    unsigned int octavesNumber;
    unsigned int levelsPerOctave;
    float initialSigma;
    float sigmaStep;
    std::vector<float> globalSigmas;
    std::vector<float> localSigmas;
    std::vector<FloatMat> space;
public:
    ScaleSpace(const FloatMat& _image, unsigned int _octavesNumber, unsigned int _levelsPerOctaveNumber,
               float initialSigma = 0.5f,
               float firstOctaveSigma = 1.6f);

    float get(int i, int j, float sigma) const;

    static float getSigmaDelta(const float prev, const float next);

    static FloatMat downscale(const FloatMat& _mat);

    std::vector<FloatMat>::const_iterator begin() const;

    std::vector<FloatMat>::const_iterator end() const;
};


}// namespace cv;




