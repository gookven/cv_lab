#include <range/v3/all.hpp>

#include "Utils.h"

#include "ScaleSpace.h"

namespace cv
{

using namespace utils;
using namespace ranges;

ScaleSpace::ScaleSpace(const FloatMat& _image, unsigned int _octavesNumber, unsigned int _levelsPerOctaveNumber,
                       float _initialSigma,
                       float _firstOctaveSigma)
        : octavesNumber(_octavesNumber)
        , levelsPerOctave(_levelsPerOctaveNumber)
        , initialSigma(_initialSigma)
        , sigmaStep(powf(2.0f, 1.0f / levelsPerOctave))
        , globalSigmas(octavesNumber * levelsPerOctave, 0.0f)
        , localSigmas(octavesNumber * levelsPerOctave, 0.0f)
        , space(octavesNumber * levelsPerOctave)
{
    assert(_firstOctaveSigma > initialSigma);

    globalSigmas[0] = _firstOctaveSigma;
    float smoothness = getSigmaDelta(initialSigma, globalSigmas[0]);
    space[0] = _image * GaussianBlurSeparable(smoothness);
    localSigmas[0] = _firstOctaveSigma;

    for (auto i : view::ints(1, int(octavesNumber * levelsPerOctave)))
    {
        globalSigmas[i] = globalSigmas[0] * powf(sigmaStep, i);

        if (i % levelsPerOctave == 0)
        {
            space[i] = downscale(space[i - 1]);
            localSigmas[i] = _firstOctaveSigma;
        }
        else
        {
            localSigmas[i] = localSigmas[i - 1] * sigmaStep;
            smoothness = getSigmaDelta(localSigmas[i - 1], localSigmas[i]);
            space[i] = space[i - 1] * GaussianBlurSeparable(smoothness);
        }
    }
}

float ScaleSpace::getSigmaDelta(const float prev, const float next)
{
    return sqrtf(next * next - prev * prev);
}

FloatMat ScaleSpace::downscale(const FloatMat& _mat)
{
    FloatMat res(_mat.getRows() / 2, _mat.getCols() / 2);

    for (auto i : view::ints(0, res.getRows()))
        for (auto j : view::ints(0, res.getCols()))
            res.at(i, j) = _mat.at(i * 2, j * 2);

    return res;
}

float ScaleSpace::get(int i, int j, float sigma) const
{
    sigma /= globalSigmas[0];
    size_t index = std::min(globalSigmas.size() - 1u, (size_t) (log2f(sigma) * levelsPerOctave));
    int downscaled = int(index / levelsPerOctave) + 1;
    return space[index].at(i / downscaled, j / downscaled);
}

std::vector<FloatMat>::const_iterator ScaleSpace::begin() const
{
    return space.begin();
}

std::vector<FloatMat>::const_iterator ScaleSpace::end() const
{
    return space.end();
}

}// namespace cv;
