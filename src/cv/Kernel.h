#pragma once

#include "ArithmeticMat.h"

namespace cv
{

class Kernel : public FloatMat
{
    const int rowOffset, colOffset;
public:

    Kernel(int _rows, int _cols, float* _data);

    Kernel(int _rows, int _cols);

    Kernel(int _rows, int _cols, const std::initializer_list<float> list);

    Kernel(FloatMat&& mat);

    FloatMat& apply(const FloatMat& mat, FloatMat& store, EdgeHandling edgeHandling = EdgeHandling::Extend) const;

    Kernel operator*(const Kernel& other) const;

    Kernel& normalize();

    Kernel& conv();

};

FloatMat operator*(const FloatMat& mat, const Kernel& kernel);

FloatMat& operator*=(FloatMat& mat, const Kernel& kernel);

}// namespace cv;




