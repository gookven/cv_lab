#pragma once

#include "Kernel.h"

namespace cv
{

namespace feature_detection
{

struct Feature
{
    float weight;
    vec2i pos;

    bool operator<(const Feature& _other) const;
};

using FeatureList = std::vector<Feature>;

FeatureList GetFeatures(const FloatMat& _weights, float _minWeight);

FeatureList GetMoravecFeatures(const FloatMat& _image, int _windowSize = 3, float _minWeight = 0.003f);

FloatMat GetMoravecWeights(const FloatMat& _image, int _windowSize);

FeatureList GetHarrisFeatures(const FloatMat& _image, int _windowSize = 3, float _minWeight = 0.01f);

FloatMat GetHarrisWeights(const FloatMat& _image, int _windowSize, float k = 0.06);

static inline bool IsLocalMaximum(const FloatMat& _weights, const vec2i& _vec, const vec2i& _wrap);

ImageMat MarkFeatures(const ImageMat& _image, const FeatureList& _features);

FeatureList FilterFeatures(const FeatureList& _featureList, int _number);

}// namespace feature_detection;

}// namespace cv;



