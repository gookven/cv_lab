#pragma once

#include <QtCore/qfileinfo.h>
#include "Kernel.h"

namespace cv
{
using SeparableKernel = std::pair<const Kernel, const Kernel>;

FloatMat operator*(const FloatMat& _mat, const SeparableKernel& _separableKernel);

namespace utils
{

constexpr float M_2_PI_F = (float) (2.0f * M_PI);
constexpr float M_SQRT_2_PI_F = sqrtf(M_2_PI_F);
constexpr float M_E_F = (float) M_E;

// Converters
QImage ToQImage(const FloatMat& mat);

FloatMat ToFloatMat(const ImageMat& mat);

ImageMat ToImageMat(const FloatMat& mat);

ImageMat FromQImage(const QImage& image);

QImage ToQImage(const ImageMat& mat);

FloatMat& Normalize(const FloatMat& mat, FloatMat& store);

FloatMat Normalize(const FloatMat& mat);

// Gaussian kernels
SeparableKernel GaussianBlurSeparable(float _smoothness);

Kernel GaussianBlur(float _smoothness);

// files and dirs
bool labDirExists(int dir, const QFileInfo& fileInfo);

bool createLabPath(int dir, const QFileInfo& fileInfo);

QString insertId(int id, int dir, const QFileInfo& fileInfo);

}// namespace utils;

}// namespace cv;