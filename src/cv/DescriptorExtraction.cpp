#include <algorithm>
#include "DescriptorExtraction.h"
#include "Utils.h"

namespace cv
{

namespace descriptor_extraction
{

using namespace ranges;

DescriptorList ExtractDescriptors(const cv::FloatMat& _floatMat, const cv::feature_detection::FeatureList& _featureList,
                                  int _subDescriptorLength, int _subWindowsPerLine, int _subWindowSize)
{
    int height = _floatMat.getRows(), width = _floatMat.getCols();

    Kernel sobelX(3, 3, {
            -1.0f, 0.0f, 1.0f,
            -2.0f, 0.0f, 2.0f,
            -1.0f, 0.0f, 1.0f
    });
    Kernel sobelY(3, 3, {
            -1.0f, -2.0f, -1.0f,
            0.0f, 0.0f, 0.0f,
            1.0f, 2.0f, 1.0f
    });

    FloatMat sobelMatX = _floatMat * sobelX.conv();
    FloatMat sobelMatY = _floatMat * sobelY.conv();
    FloatMat sobelGradientMagnitude = sobelMatX ^ sobelMatY;
    FloatMat sobelGradientDirection = GradientDirection(sobelMatX, sobelMatY);

    vec2i wrapper(height, width);

    DescriptorList result;
    auto windowSize = _subWindowSize * _subWindowsPerLine;

    for (auto&& feature : _featureList)
    {
        auto basePos = feature.pos - vec2i(windowSize / 2, windowSize / 2);
        auto directionDescriptor = ExtractDescriptor(feature.pos, 0.0f, basePos, windowSize, wrapper,
                _subDescriptorLength, sobelGradientDirection, sobelGradientMagnitude);
        auto mainDirections = ExtractDirections(directionDescriptor);

        for (auto&& directionAngle : mainDirections)
        {
            Descriptor descriptor(_subDescriptorLength * _subWindowsPerLine * _subWindowsPerLine);
            descriptor.feature = feature;
            descriptor.angle = directionAngle;
            int subDescriptorOffset = 0;
            for (auto i : view::ints(0, _subWindowsPerLine))
            {
                for (auto j : view::ints(0, _subWindowsPerLine))
                {
                    auto subDescriptor = ExtractDescriptor(feature.pos, directionAngle,
                            basePos + vec2i{_subWindowSize * i, _subWindowSize * j}, _subWindowSize, wrapper,
                            _subDescriptorLength, sobelGradientDirection,
                            sobelGradientMagnitude);
                    std::copy(subDescriptor.begin(), subDescriptor.end(), descriptor.begin() + subDescriptorOffset);
                    subDescriptorOffset += _subDescriptorLength;
                }
            }

            result.push_back(descriptor.normalize());
        }
    }

    return result;
}

Descriptor ExtractDescriptor(const vec2i& _center, float rotateAngle, const vec2i& _basePos, int _windowSize,
                             const vec2i& _wrapper, int _descriptorLength,
                             const FloatMat& _angles, const FloatMat& _magnitudes)
{
    Descriptor descriptor(_descriptorLength);

    float angleStep = utils::M_2_PI_F / _descriptorLength;

    for (auto i : view::ints(0, _windowSize))
    {
        for (auto j : view::ints(0, _windowSize))
        {
            auto pos = _basePos + vec2i(i, j);
            pos = ((pos - _center).rotate(rotateAngle) + _center).wrap(_wrapper);

            float magnitude = _magnitudes.at(pos);
            float angle = std::fmod(_angles.at(pos) - rotateAngle, utils::M_2_PI_F);
            if (angle < 0.0f) angle += utils::M_2_PI_F;

            // get bucket index
            int index = int(angle / angleStep);

            // magnitude split proportions
            float part = (angle - (angleStep * index)) / angleStep;

            descriptor[index % _descriptorLength] += magnitude * (1.0f - part);
            descriptor[(index + 1) % _descriptorLength] += magnitude * part;
        }
    }

    return descriptor;
}

DescriptorPairList GetCorrespondingPairs(const DescriptorList& _firstList,
                                         const DescriptorList& _secondList, float _threshold)
{
    assert(_firstList.size() && _secondList.size() && "Descriptor lists must be non-empty!");
    DescriptorPairList pairs;

    for (auto&& desc : _firstList)
    {
        // get two closest elements
        auto closestForward = GetTwoClosestDescriptors(desc, _secondList);
        auto closestBackward = GetTwoClosestDescriptors(closestForward.first, _firstList);

        if (closestBackward.first == desc
            && desc.distance(closestForward.second) > desc.distance(closestForward.first) * _threshold
            && closestForward.first.distance(closestBackward.second) >
               closestForward.first.distance(closestBackward.first) * _threshold)
        {
            pairs.emplace_back(closestBackward.first, closestForward.first);
        }
    }

    return pairs;
}

float GetInterpolatedXAxisCenter(const std::array<float, 3>& x, const std::array<float, 3>& y, float sqXStep)
{
    std::array<float, 3> a{
            y[0] / (2.0f * sqXStep),
            -y[1] / sqXStep,
            y[2] / (2.0f * sqXStep)
    };
    std::array<float, 3> b{
            -(x[1] + x[2]) * a[0],
            -(x[0] + x[2]) * a[1],
            -(x[0] + x[1]) * a[2]
    };

    float as = 2.0f * (a[0] + a[1] + a[2]);
    float bs = b[0] + b[1] + b[2];

    float angle = std::fmod(-bs / as, utils::M_2_PI_F);
    if (angle < 0.0f) angle += utils::M_2_PI_F;

    return angle;
}

std::vector<float> ExtractDirections(const Descriptor& _descriptor)
{
    std::vector<float> res;

    int descLen = _descriptor.size();
    float angleStep = utils::M_2_PI_F / descLen;
    float sqAngleStep = angleStep * angleStep;

    auto max = std::max_element(_descriptor.begin(), _descriptor.end());
    auto index1 = int(max - _descriptor.begin());
    float angle1 = GetInterpolatedXAxisCenter({
            (index1 - 1) * angleStep,
            index1 * angleStep,
            (index1 + 1) * angleStep
    }, {
            _descriptor[(index1 - 1 + descLen) % descLen],
            _descriptor[index1],
            _descriptor[(index1 + 1) % descLen]
    }, sqAngleStep);

    res.push_back(angle1);

    // finding second possible peak
    float maxHeight = 0.0f;
    int index2 = -1;
    for (auto i : view::ints(0, descLen))
    {
        if (i == index1) continue;
        int l = (i - 1 + descLen) % descLen, r = (i + 1) % descLen;
        if (_descriptor[i] > maxHeight && _descriptor[i] >= _descriptor[l] && _descriptor[i] >= _descriptor[r])
        {
            maxHeight = _descriptor[i];
            index2 = i;
        }
    }

    if (index2 != -1 && _descriptor[index1] * 0.8f < _descriptor[index2])
    {
        float angle2 = GetInterpolatedXAxisCenter({
                (index2 - 1) * angleStep,
                index2 * angleStep,
                (index2 + 1) * angleStep
        }, {
                _descriptor[(index2 - 1 + descLen) % descLen],
                _descriptor[index2],
                _descriptor[(index2 + 1) % descLen]
        }, sqAngleStep);
        res.push_back(angle2);
    }

    return res;
}

DescriptorPair GetTwoClosestDescriptors(const Descriptor& _desc, const DescriptorList& _list)
{
    auto dummy = *(_list.begin());
    auto distance = _desc.distance(dummy);
    auto closest = std::make_pair(dummy, dummy);
    auto closestDistance = std::make_pair(distance, distance);
    for (auto&& second : _list)
    {
        auto d = second.distance(_desc);
        if (d <= closestDistance.first)
        {
            std::swap(closestDistance.first, closestDistance.second);
            closestDistance.first = d;
            std::swap(closest.first, closest.second);
            closest.first = second;
        }
        else if (d <= closestDistance.second)
        {
            closestDistance.second = d;
            closest.second = second;
        }
    }

    return closest;
}


}// namespace descriptor_extraction;

}// namespace cv;


