#include <QCoreApplication>
#include <QImage>

#include <iostream>
#include <QtCore/qcommandlineparser.h>
#include <QtCore/qfileinfo.h>
#include <QtGui/QPainter>

#include "cv/common.h"
#include "cv/Utils.h"
#include "cv/FeatureDetection.h"
#include "cv/DescriptorExtraction.h"

using namespace cv;
using namespace cv::feature_detection;
using namespace cv::descriptor_extraction;

constexpr int LabId = 4;

FloatMat MapFeatures(const vec2i& size, FeatureList& features)
{
    FloatMat floatMat(size.i, size.j);
    floatMat = 0.0f;
    for (auto&& feature : features)
    {
        floatMat.at(feature.pos) = feature.weight;
    }
    return floatMat;
}

QImage MarkCorrespondingFeatures(QImage& _img1, QImage& _img2, DescriptorPairList& _pairs)
{
    auto s1 = _img1.size();
    auto s2 = _img2.size();
    QImage result(s1.width() + s2.width(), std::max(s1.height(), s2.height()), QImage::Format_RGBA8888);
    QPainter painter(&result);
    painter.drawImage(0, 0, _img1);
    painter.drawImage(s1.width(), 0, _img2);

    std::random_device rand;

    for (auto&& pair : _pairs)
    {
        auto p1 = pair.first.feature.pos;
        auto p2 = pair.second.feature.pos + vec2i{0, s1.width()};
        painter.setPen(QColor::fromRgb((QRgb) rand()));
        painter.drawLine(p1.x, p1.y, p2.x, p2.y);
    }

    return result;
}

void execute(const QString& image_path1, const QString& image_path2)
{
    QFileInfo fileInfo1(image_path1);
    if (!fileInfo1.exists())
    {
        std::cerr << "No such file: " + image_path1.toStdString() << std::endl;
        return;
    }

    QImage image1(image_path1);
    if (image1.isNull())
    {
        std::cerr << "Couldn't open image1: " + image_path1.toStdString() << std::endl;
        return;
    }

    QFileInfo fileInfo2(image_path2);
    if (!fileInfo2.exists())
    {
        std::cerr << "No such file: " + image_path2.toStdString() << std::endl;
        return;
    }

    QImage image2(image_path2);
    if (image2.isNull())
    {
        std::cerr << "Couldn't open image2: " + image_path2.toStdString() << std::endl;
        return;
    }

    if (!utils::labDirExists(LabId, fileInfo1))
    {
        if (!utils::createLabPath(LabId, fileInfo1))
        {
            std::cerr << "Couldn't create output directory!" << std::endl;
            return;
        }
    }

    ImageMat imageMat1 = utils::FromQImage(image1);
    FloatMat floatMat1 = utils::ToFloatMat(imageMat1);

    auto harrisFeatures1 = FilterFeatures(GetHarrisFeatures(floatMat1), 100);
    auto imDesc1 = utils::ToQImage(MarkFeatures(imageMat1, harrisFeatures1));
    imDesc1.save(utils::insertId(1, LabId, fileInfo1));
    auto descriptors1 = ExtractDescriptors(floatMat1, harrisFeatures1);

    ImageMat imageMat2 = utils::FromQImage(image2);
    FloatMat floatMat2 = utils::ToFloatMat(imageMat2);
    auto harrisFeatures2 = FilterFeatures(GetHarrisFeatures(floatMat2), 100);
    auto imDesc2 = utils::ToQImage(MarkFeatures(imageMat2, harrisFeatures2));
    imDesc2.save(utils::insertId(1, LabId, fileInfo2));
    auto descriptors2 = ExtractDescriptors(floatMat2, harrisFeatures2);

    auto pairs = GetCorrespondingPairs(descriptors1, descriptors2);

    MarkCorrespondingFeatures(imDesc1, imDesc2, pairs).save(utils::insertId(128, LabId, fileInfo1));

    utils::ToQImage(floatMat1).save(utils::insertId(255, LabId, fileInfo1));
    image1.save(utils::insertId(0, LabId, fileInfo1));
}

int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;

    parser.addPositionalArgument("image", "image to process");

    auto args = a.arguments();

    if (!parser.parse(args))
    {
        std::cerr << parser.errorText().toStdString() << std::endl;
        return -1;
    }

    auto positionalArguments = parser.positionalArguments();
    if (positionalArguments.size() < 2)
    {
        std::cerr << "Two images should be provided!" << std::endl;
        return -1;
    }

    execute(positionalArguments[0], positionalArguments[1]);

    return 0;
}
