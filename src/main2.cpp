#include <QCoreApplication>
#include <QImage>

#include <iostream>
#include <QtCore/qcommandlineparser.h>
#include <QtCore/qfileinfo.h>

#include "cv/common.h"
#include "cv/Utils.h"
#include "cv/ScaleSpace.h"

using namespace cv;

constexpr int LabId = 2;

void execute(const QString& image_path, unsigned int octaves, unsigned int levels, float sigma, float sigma1)
{
    QFileInfo fileInfo(image_path);

    if (!fileInfo.exists())
    {
        std::cerr << "No such file: " + image_path.toStdString() << std::endl;
        return;
    }

    QImage image(image_path);

    if (image.isNull())
    {
        std::cerr << "Couldn't open image: " + image_path.toStdString() << std::endl;
        return;
    }

    if (!utils::labDirExists(LabId, fileInfo))
    {
        if (!utils::createLabPath(LabId, fileInfo))
        {
            std::cerr << "Couldn't create output directory!" << std::endl;
            return;
        }
    }

    FloatMat imageMat = utils::ToFloatMat(utils::FromQImage(image));

    ScaleSpace scaleSpace(imageMat, octaves, levels, sigma, sigma1);

    int index = 0;
    for (auto&& mat : scaleSpace)
    {
        utils::ToQImage(mat).save(utils::insertId(index++, LabId, fileInfo));
    }
}

/*
 * run the app as described below, multiple images are handled
 * ./cv_lab2 ../resources/image.jpg -o 3 -l 4
 */
int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;

    parser.addOptions({
            {"o",  "number of octaves",           "octaves", "2"},
            {"l",  "number of levels per octave", "levels",  "2"},
            {"s",  "initial image sigma",         "sigma",   "0.5"},
            {"s1", "next image sigma",            "sigma1",  "1.6"},
    });

    parser.addPositionalArgument("image", "image to process");

    auto args = a.arguments();

    if (!parser.parse(args))
    {
        std::cerr << parser.errorText().toStdString() << std::endl;
        return -1;
    }

    auto positionalArguments = parser.positionalArguments();
    if (positionalArguments.isEmpty())
    {
        std::cerr << "No image provided!" << std::endl;
        return -1;
    }

    bool parsedCorrectly = true;
    unsigned int octaves = parser.value("o").toUInt(&parsedCorrectly);
    unsigned int levels = parser.value("l").toUInt(&parsedCorrectly);
    float sigma = parser.value("s").toFloat(&parsedCorrectly);
    float sigma1 = parser.value("s1").toFloat(&parsedCorrectly);

    if (!parsedCorrectly)
    {
        std::cerr << "Wrong/incorrect arguments provided!" << std::endl;
        return -1;
    }

    for (auto&& image : positionalArguments)
    {
        execute(image, octaves, levels, sigma, sigma1);
    }

    return 0;
}
