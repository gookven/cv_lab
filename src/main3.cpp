#include <QCoreApplication>
#include <QImage>

#include <iostream>
#include <QtCore/qcommandlineparser.h>
#include <QtCore/qfileinfo.h>

#include "cv/common.h"
#include "cv/Utils.h"
#include "cv/FeatureDetection.h"

using namespace cv;
using namespace cv::feature_detection;

constexpr int LabId = 3;

FloatMat MapFeatures(const vec2i& size, FeatureList& features)
{
    FloatMat floatMat(size.i, size.j);
    floatMat = 0.0f;
    for (auto&& feature : features)
    {
        floatMat.at(feature.pos) = feature.weight;
    }
    return floatMat;
}

void execute(const QString& image_path)
{
    QFileInfo fileInfo(image_path);

    if (!fileInfo.exists())
    {
        std::cerr << "No such file: " + image_path.toStdString() << std::endl;
        return;
    }

    QImage image(image_path);

    if (image.isNull())
    {
        std::cerr << "Couldn't open image: " + image_path.toStdString() << std::endl;
        return;
    }

    if (!utils::labDirExists(LabId, fileInfo))
    {
        if (!utils::createLabPath(LabId, fileInfo))
        {
            std::cerr << "Couldn't create output directory!" << std::endl;
            return;
        }
    }

    ImageMat imageMat = utils::FromQImage(image);
    FloatMat floatMat = utils::ToFloatMat(imageMat);
    utils::ToQImage(floatMat).save(utils::insertId(255, LabId, fileInfo));

    vec2i matSize(floatMat.getRows(), floatMat.getCols());

    auto featuresMoravec = feature_detection::GetMoravecFeatures(floatMat, 3, 0.003f);
    auto floatMatMoravec = MapFeatures(matSize, featuresMoravec);

    auto featuresHarris = feature_detection::GetHarrisFeatures(floatMat, 3, 0.01f);
    auto floatMatHarris = MapFeatures(matSize, featuresHarris);

    auto featuresMoravecFiltered = feature_detection::FilterFeatures(featuresMoravec, 100);
    auto floatMatMoravecFiltered = MapFeatures(matSize, featuresMoravecFiltered);

    auto featuresHarrisFiltered = feature_detection::FilterFeatures(featuresHarris, 100);
    auto floatMatHarrisFiltered = MapFeatures(matSize, featuresHarrisFiltered);

    utils::ToQImage(imageMat).save(utils::insertId(254, LabId, fileInfo));

    utils::ToQImage(floatMatMoravec)
            .save(utils::insertId(1, LabId, fileInfo));
    utils::ToQImage(feature_detection::MarkFeatures(imageMat, featuresMoravec))
            .save(utils::insertId(2, LabId, fileInfo));
    utils::ToQImage(floatMatMoravecFiltered)
            .save(utils::insertId(3, LabId, fileInfo));
    utils::ToQImage(feature_detection::MarkFeatures(imageMat, featuresMoravecFiltered))
            .save(utils::insertId(4, LabId, fileInfo));


    utils::ToQImage(floatMatHarris)
            .save(utils::insertId(11, LabId, fileInfo));
    utils::ToQImage(feature_detection::MarkFeatures(imageMat, featuresHarris))
            .save(utils::insertId(12, LabId, fileInfo));
    utils::ToQImage(floatMatHarrisFiltered)
            .save(utils::insertId(13, LabId, fileInfo));
    utils::ToQImage(feature_detection::MarkFeatures(imageMat, featuresHarrisFiltered))
            .save(utils::insertId(14, LabId, fileInfo));

    image.save(utils::insertId(0, LabId, fileInfo));
}

int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;

    parser.addPositionalArgument("image", "image to process");

    auto args = a.arguments();

    if (!parser.parse(args))
    {
        std::cerr << parser.errorText().toStdString() << std::endl;
        return -1;
    }

    auto positionalArguments = parser.positionalArguments();
    if (positionalArguments.isEmpty())
    {
        std::cerr << "No image provided!" << std::endl;
        return -1;
    }

    for (auto&& image : positionalArguments)
    {
        execute(image);
    }

    return 0;
}
