#include <QCoreApplication>
#include <QImage>

#include <iostream>
#include <QtCore/qfileinfo.h>

#include "cv/common.h"
#include "cv/Utils.h"

using namespace cv;

constexpr int LabId = 1;

void execute(const QString& image_path)
{
    QFileInfo fileInfo(image_path);

    if (!fileInfo.exists())
    {
        std::cerr << "No such file: " + image_path.toStdString() << std::endl;
        return;
    }

    QImage image(image_path);

    if (image.isNull())
    {
        std::cerr << "Couldn't open image: " + image_path.toStdString() << std::endl;
        return;
    }

    if (!utils::labDirExists(LabId, fileInfo))
    {
        if (!utils::createLabPath(LabId, fileInfo))
        {
            std::cerr << "Couldn't create output directory!" << std::endl;
            return;
        }
    }

    ImageMat mat = utils::FromQImage(image);

    Kernel sobelX(3, 3, {
            -1.0f, 0.0f, 1.0f,
            -2.0f, 0.0f, 2.0f,
            -1.0f, 0.0f, 1.0f
    });
    Kernel sobelY(3, 3, {
            -1.0f, -2.0f, -1.0f,
            0.0f, 0.0f, 0.0f,
            1.0f, 2.0f, 1.0f
    });

    FloatMat floatMat = utils::ToFloatMat(mat);
    FloatMat sobelMatX = floatMat * sobelX.conv();
    FloatMat sobelMatY = floatMat * sobelY.conv();

    utils::ToQImage(mat).save(utils::insertId(255, LabId, fileInfo));

    FloatMat sobelGradientMagnitude = sobelMatX ^sobelMatY;
    FloatMat sobelGradientDirection = GradientDirection(sobelMatX, sobelMatY);

    Kernel gauss = utils::GaussianBlur(2.0f);
    FloatMat blured = floatMat * gauss;

    auto gaussSeparable = utils::GaussianBlurSeparable(2.0f);
    FloatMat blured1 = floatMat * gaussSeparable;

    utils::ToQImage(utils::Normalize(sobelMatX)).save(utils::insertId(0, LabId, fileInfo));
    utils::ToQImage(utils::Normalize(sobelMatY)).save(utils::insertId(1, LabId, fileInfo));
    utils::ToQImage(utils::Normalize(sobelGradientMagnitude)).save(utils::insertId(2, LabId, fileInfo));
    utils::ToQImage(sobelGradientDirection).save(utils::insertId(3, LabId, fileInfo));
    utils::ToQImage(blured).save(utils::insertId(4, LabId, fileInfo));
    utils::ToQImage(blured1).save(utils::insertId(5, LabId, fileInfo));
}

int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);

    auto args = a.arguments();
    args.pop_front();
    for (auto path : args)
    {
        execute(path);
    }

    return 0;
}
